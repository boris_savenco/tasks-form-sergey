/*
  Дана матрица,
  каждая строка которой отсортированы справа налево и каждый столбец
  отсортированы сверху вниз по возрастанию.
  Написать функцию, которая определит входит ли указанное число в данную
  матрицу.
 */


/**
 * Проверить содержит ли отсортрованная матрица число
 * @param  {Array} array Матрица для поиска
 * @param  {Number} num  Искомое число
 * @return {Boolean}
 */
function searchIndexInSortMatrix(array, num){
    var matrixLen = array.length,
        tArr,
        i = 0;

    for(;i < matrixLen && array[i][0] <= num; ++i){
        tArr = array[i];
        if (tArr[tArr.length - 1] >= num){
            if (searchIndexInSortArray(tArr, num)) return true;
        }
    }

    return false;
}

/**
 * Проверить содержит ли сортированный массив число
 * @param  {Array} array  сортированный массив
 * @param  {Numger} num   число для поиска
 * @return {Boolean}      Индекс вхождения или -1
 */
function searchIndexInSortArray(array, num){
    var arrLen = array.length,
     i=0;

    if(array[arrLen - 1] >= num){

        for(;++i < arrLen && array[i] <= num;){
            if (array[i] === num) return true;
        }

    }
    return false;
}

/**
 * Тесты
 */
var tMatrix = [
    [1,2,8,9],
    [2,4,9,12],
    [4,7,10,13],
    [6,8,11,15]
];

console.log(searchIndexInSortMatrix(tMatrix, 7), 7);
console.log(searchIndexInSortMatrix(tMatrix, 5), 5);
