/*
Написать функцию, которая
на вход получает последовательность слов, разделенных пробелами,
а на выходе возвращает последовательность этих же слов только
в обратном порядке.
 */

/**
 * Вернуть последовательность слов в обратном порядке
 * @param  {String} sentence предложение
 * @return {String}          предложение, в котором слова идут в обратном порядке
 */
function reversSentence(sentence){
    var arrWords = sentence.split(' ');

    return arrWords.sort(function(){
        return 1;
    }).join(' ');
}



/**
 * Тесты
 */

var arrTestStr = [
    'one two three',
    'one two three four five',
    'one two    three  four five',
    'При решении задачи необходимо обращать внимание на сложность используемого алгоритма.'
], tStr, tRevStr;

for (var i = 0; i < arrTestStr.length; i++){
    tStr = arrTestStr[i];
    tRevStr = reversSentence(tStr);
    console.log(tRevStr, tStr === reversSentence(tRevStr));
}
