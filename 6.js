/**
 * На вход функции поступает число
 * в десятичном виде, и система исчисления (двоичная, восьмеричная ...)
 * на выходе должно быть строковое представление числа в указанной
 * системе исчисления.
 */

//Нативный способ
/**
 * Перевести число в строку в указанной системе счислений
 * @param  {Number} decInt Число
 * @param  {Number} sysInt Система счислений
 * @return {String}        число в системе счислений
 */
function decToStr(decInt, sysInt){
    return decInt.toString(sysInt);
}

//Кастомный способ, позволяет использовать системы счисление до 36
/**
 * Перевести число в строку в указанной системе счислений
 * @param  {Number} decInt Число
 * @param  {Number} sysInt Система счислений
 * @return {String}        число в системе счислений
 */
function decToStrCustom(decInt, sysInt){
    var alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        result = '';
        
    if (sysInt > alphabet.length) throw "Слишком большая система счислений";
    while (decInt > 0){
        result = alphabet[decInt % sysInt] + result;
        decInt = parseInt(decInt / sysInt);
    }
    return result;
}

/*
    тесты
 */

var n = getRndNumBetween(0, 1000),
    s = getRndNumBetween(2, 32);
console.log('custom', decToStrCustom(n, s), n, s);
console.log('native', decToStr(n, s), n, s);

/**
 * Получить натуральное случайное число между n1 и n2
 * @param  {Number} n1 минимальное значение
 * @param  {Number} n2 максимальное значение
 * @return {Number}    Натуральное, случайное число
 */
function getRndNumBetween(n1, n2){
    return parseInt((n2 - n1) * Math.random() + n1);
}
