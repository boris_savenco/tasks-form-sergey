/**
 * Проверить, является ли число палиндромом
 */

/**
 * Является ли число полиндромом
 * @param  {Number}  num целое число
 * @return {Boolean}     является ли полиндромом
 */
function isMathPolyndrom(num){
    var rev = 0,
        n = num,
        int, dig;

    for (; n > 0 ;){

        int = parseInt(n / 10);
        dig = n - int * 10;
        n = int;

        rev = rev * 10 + dig;
    }

    return num === rev;
}

/**
 * Тест
 */

 var arrNum = [
    11,
    1111,
    1221,
    12345654321,
    123123,
    122334433221
 ], tNum;

 for (var i = 0; i < arrNum.length; i++){
    tNum = arrNum[i];
    console.log(isMathPolyndrom(tNum), tNum);
 }
