/**
 * Дан сортированный массив и число.
 * Необходимо найти индекс вхождения числа в этом массиве.
 * Сложность алгоритма не должна превышать O(log n)
 */

/**
 * получить индекс вхождения числа в сортированный массив
 * @param  {Array} array  сортированный массив
 * @param  {Numger} num   число для поиска
 * @return {Numger}       Индекс вхождения или -1
 */
function getIndexInSortArray(array, num){
    var arrLen = array.length,
        i=0;

    if(array[arrLen - 1] >= num){

        for(;++i < arrLen && array[i] <= num;){
            if (array[i] === num) return i;
        }

    }

    return -1;
}


//тесты
var a1 = [-10,3,4,7,8,15,22,44,17],
    a2 = [0,1,2,3,4,5,6,7,8,9,12,22,44,55],
    a3 = [6,15,55,78,94],
    n1 = 6,
    n2 = 55;

console.log(getIndexInSortArray(a1, n1), a1, n1);
console.log(getIndexInSortArray(a2, n1), a2, n1);
console.log(getIndexInSortArray(a3, n1), a3, n1);
console.log('---')
console.log(getIndexInSortArray(a1, n2), a1, n2);
console.log(getIndexInSortArray(a2, n2), a2, n2);
console.log(getIndexInSortArray(a3, n2), a3, n2);
