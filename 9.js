/**
 * Необходимо написать алгоритм
 * соединения двух сортированных массивов в один,
 * сохраняя сортировку получаемого массива.
 */

/**
 * Объединить 2 сортированных массива сохраняя сортировку
 * @param  {Array} arr1 сортированный массив 1
 * @param  {Array} arr2 сортированный массив 2
 * @return {Array}      объединенный массив
 */
function concatSortArr(arr1, arr2){
    var retArr = [],
        i1 = i2 = 0;
    //заполнить новый массив, пока не дойдем до конца одного из массивов
    for (; i1 < arr1.length && i2 < arr2.length;){
        if(arr1[i1] < arr2[i2]){
            retArr.push(arr1[i1++]);
        } else {
            retArr.push(arr2[i2++]);
        }
    }
    //заполнить оставшимися элементами одного из массивов
    if(i1 < arr1.length){
        retArr = retArr.concat(arr1.slice(i1));
    } else if ( i2 < arr2.length){
        retArr = retArr.concat(arr2.slice(i2));
    }

    return retArr;
}


/**
 * Тесты
 */

var a1 = [-5, -2, 1, 3, 6, 7, 10, 28, 55, 64, 92, 128, 256],
    a2 = [5,6,7,8,10,25,34,41,96,97,98,99,100,257,258,259],
    a3 = [-1, 0, 1,2,3,4,5,6,7,8,9,10,10000];

console.log(concatSortArr(a1,a2));
console.log(concatSortArr(a2,a1));
console.log(concatSortArr(a2,a3));
