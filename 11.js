/**
 * Дана строка.
 * Написать функцию, которая найдет все возможные
 * перестановки букв данной строки
 */

//http://progpage.narod.ru/alg/per.html
function getAllVariants(str){

    var
        result = [''],
        per = [],
        obr = [],
        n = str.length,
        flag, i, j, k, tmp, min, raz;

    for (i = 0; i < n; i++){
        per[i] = i + 1;
    }

	while (1) {
        for (i = 0; i < n; i++){
            result[result.length - 1] += str[per[i]-1];
        }

        flag = false;

		for (i = n - 2; i >= 0; i--) {
			if (per[i] < per[i + 1]) {
				flag = true;
				break;
			}
		}
		if (flag == false) {
			break;
		}
        result.push('');
        raz = per[i+1];
		for (j = i+1; j < n; j++) {
			if (((per[j] - per[i]) < raz) && (per[i] < per[j])) {
				min = j;
			}
		}

		tmp	= per[i];
		per[i] 	   = per[min];
		per[min] = tmp;

        for (j = i + 1; j < n; j++) {
			obr[j] = per[j];
		}
		j = i + 1;
		for (k = n-1; k >= i+1; k--) {
			per[j] = obr[k];
			j++;
		}

	}
	return result;
}

// первая попытка
// Работает для строк длинной <= 3
// function getAllVariants(str){
//     var varArr = [],
//         arrS = str.split(''),
//         strL = arrS.length,
//         buf, d;
//
//     for (var x = 0; x < strL; x++){
//         for (var i = 0; i < strL - 1; i++){
//             buf = arrS[i];
//             arrS[i] = arrS[i + 1];
//             arrS[i + 1] = buf;
//             varArr.push(arrS.join(''));
//         }
//     }
//
//     return varArr;
// }

// Рекурсивный вариант
// function getAllVariants(str, prefix) {
//     var
//         prefix = prefix || '',
//         n = str.length,
//         variants = [];
//     if (n == 0){
//         return prefix;
//     } else {
//         for (var i = 0; i < n; i++)
//             variants = variants.concat(getAllVariants(str.substring(0, i) + str.substring(i+1, n), prefix + str[i]));
//     }
//     return variants;
// }

/**
 * тесты
 */

var arrTest = [
    'abc',
    'abcd',
    'abcdef',
], l, tstr, fac, variants;

for(l = 0; l < arrTest.length; l++){
    tstr = arrTest[l];
    fac = factorial(tstr.length);
    variants = getAllVariants(tstr);

    console.log(fac, variants.length, tstr, variants);

}

function factorial(n){
    var ret = 1;
    for (var i = 1; i<=n; i++){
        ret *= i;
    }
    return ret;
}
