/**
 * На вход функции поступает
 * число в строковом представлении и система исчисления данного числа.
 * Функция должна возвращать число в десятичной системе исчисления.
 */

//Нативный способ

/**
 * Перевести число в десятичную систему счислений
 * @param  {String} strNum число
 * @param  {Number} sysInt система счислений
 * @return {Numper}        число в десятичной системе счислений
 */
function toDec(strNum, sysInt){
    return parseInt(strNum.toLowerCase(), sysInt);
}

//Кастомный способ, позволяет использовать системы счисление до 36
/**
 * Перевести число в десятичную систему счислений
 * @param  {String} strNum число
 * @param  {Number} sysInt система счислений
 * @return {Numper}        число в десятичной системе счислений
 */
function toDecCustom(strNum, sysInt){
    var alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        result = 0;

    strNum = ('' + strNum).toUpperCase();
    if (sysInt > alphabet.length) throw "Слишком большая система счислений";

    for (var i = 0; i < strNum.length; i++){
        result = result * sysInt + alphabet.indexOf(strNum[i]);
    }

    return result;
}
/*
    тесты
 */
//
    var n = getRndNumBetween(1,1000),
        s = getRndNumBetween(1,36),
        nInS = decToStrCustom(n, s);

console.log('custom', toDecCustom(nInS, s), n, s, nInS);

/**
 * Получить натуральное случайное число между n1 и n2
 * @param  {Number} n1 минимальное значение
 * @param  {Number} n2 максимальное значение
 * @return {Number}    Натуральное, случайное число
 */
function getRndNumBetween(n1, n2){
    return parseInt((n2 - n1) * Math.random() + n1);
}

/**
 * Перевести число в строку в указанной системе счислений
 * @param  {Number} decInt Число
 * @param  {Number} sysInt Система счислений
 * @return {String}        число в системе счислений
 */
function decToStrCustom(decInt, sysInt){
    var alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        result = '';

    if (sysInt > alphabet.length) throw "Слишком большая система счислений";
    while (decInt > 0){
        result = alphabet[decInt % sysInt] + result;
        decInt = parseInt(decInt / sysInt);
    }
    return result;
}
