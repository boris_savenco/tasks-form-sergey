/**
 * Дано число n, определить n-ый элемент последовательности фибоначчи
 */

/**
 * Получить число фибоначи числа для целого, положительного числа
 * @param  {Number} n номер числа в последовательности
 * @return {Number}   число фибоначи
 */
function getFib(n){
    var fib = [0,1];
    if (n < 0 || n !== parseInt(n)) return false;
    for (var i = 2; i <= n; i++){
        fib[i] = fib[i-1] + fib[i-2];
    }
    return fib[n];
}
/**
 * тесты
 */
console.log(getFib(0), 0);
console.log(getFib(2), 2);
console.log(getFib(7), 7);
console.log(getFib(10), 10);
console.log(getFib(100), 100);
console.log(getFib(1200), 1200);
