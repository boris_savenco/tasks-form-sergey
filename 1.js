/**
 * Дана строка и подстрока.
 * Написать функцию, которая будет искать первое вхождение подстроки
 * в строке.
 */

/**
 * Поиск вхождения подстроки в строку
 * @param  {String} text                строка, в которой производится поиск
 * @param  {String} subString           строка, которую нужно найти
 * @param  {Number} [num=0]             Номер вхождения, -1 - последний
 * @param  {Boolean} [softCase=false]   не чувствителен к регистру
 * @return {Number}                     номер символа вхождения
 */
function getIndexOf(text, subString, num, softCase){
    var num = num || 0,
        i=0,
        realLastPos, tempLastPos;

    if(softCase){
        text = text.toLowerCase();
        subString = subString.toLowerCase();
    }

    realLastPos = tempLastPos = text.indexOf(subString);
    while (i !== num && tempLastPos !== -1){
        tempLastPos = text.indexOf(subString, tempLastPos + 1);
        if(tempLastPos !== -1) realLastPos = tempLastPos;
        i++;
    }

    return (i === num || num === -1) ? realLastPos : -1;
}

// Тесты
var t1 = 'asdsasd asdd asd asdasddsfgdfsg asdf',
    ss1 = 'dd',
    ss2 = 'DD';
console.log(getIndexOf(t1, ss1), t1, ss1)
console.log(getIndexOf(t1, ss2), t1, ss2)
console.log(getIndexOf(t1, ss2, 1, true), '1, true', t1, ss2)
console.log(getIndexOf(t1, ss2, -1, true), '-1, true', t1, ss2)
